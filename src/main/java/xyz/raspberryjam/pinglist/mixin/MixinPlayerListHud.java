package xyz.raspberryjam.pinglist.mixin;

import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.gui.DrawContext;
import net.minecraft.client.gui.hud.PlayerListHud;
import net.minecraft.client.network.PlayerListEntry;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.*;

@Environment(EnvType.CLIENT)
@Mixin(PlayerListHud.class)
public class MixinPlayerListHud {
    /**
     * As of yarn 1.18.1+build.2, this variable corresponds to the width of the list.
     * @param n Width of list
     * @return Modified magic number
     */
    @ModifyVariable(method = "render", at = @At(value = "STORE"), ordinal = 7)
    private int modifyN(int n) {
        return n + MinecraftClient.getInstance().textRenderer.getWidth("9999ms");
    }

    /**
     * Replaces the usual connection bar icon with a stylised numerical value.
     * @param playerListHud Mixin value
     * @param drawContext Mixin value
     * @param width Mixin value
     * @param x Mixin value
     * @param y Mixin value
     * @param playerListEntry Mixin value
     */
    @Redirect(method = "render", at = @At(value = "INVOKE", target = "Lnet/minecraft/client/gui/hud/PlayerListHud;renderLatencyIcon(Lnet/minecraft/client/gui/DrawContext;IIILnet/minecraft/client/network/PlayerListEntry;)V"))
    private void renderLatencyIcon(PlayerListHud playerListHud, DrawContext drawContext, int width, int x, int y, PlayerListEntry playerListEntry) {
        MinecraftClient client = MinecraftClient.getInstance();
        int latency = playerListEntry.getLatency();
        int color = 0x66ff88; // Green
        if(latency > 300) {
            color = 0xff5252; // Red
        } else if (latency > 150) {
            color = 0xffba52; // Orange
        }
        String strLatency = latency + "ms";
        int strOffset = client.textRenderer.getWidth(strLatency);
        drawContext.drawTextWithShadow(client.textRenderer, strLatency, x + width - strOffset, y, color);
    }
}
