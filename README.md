# PingList

A minimal minecraft fabric mod to display numerical ping values instead of the usual connection bars.

### Find releases here:
- Modrinth: https://modrinth.com/mod/pinglist
- CurseForge: https://www.curseforge.com/minecraft/mc-mods/pinglist
